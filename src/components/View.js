import React, { Component } from 'react';
import axios from 'axios';

export default class View extends Component {
    constructor(props){
        super(props);
        this.state={
            data:{
                DATA:[]
            },
            TITLE:'',
            DESCRIPTION:'',
            IMAGE:'',

        }
    }
    componentWillMount(){
        const ID = this.props.match.params.id;
        console.log(this.props.match.params.id);
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${ID}`).then(res=>{  
            this.setState({
                data: res.data.DATA,
                TITLE: res.data.DATA.TITLE,
                DESCRIPTION: res.data.DATA.DESCRIPTION,
                IMAGE: res.data.DATA.IMAGE

            }) ;
            console.log(this.state.data)
            console.log(this.state.TITLE)
            console.log(this.state.DESCRIPTION)
                    
            
        })
    }
    render() {
        return (
            <div className ="container">
                <br/><br/>
                <h2 >{this.state.TITLE}</h2>
                <br/>
                <img src={this.state.IMAGE} style={{width:"500px", height:"300px"}}/>
                <br/><br/>
                <p>{this.state.DESCRIPTION}</p>
                
            </div>
        )
    }
}

