import React, { Component } from 'react';
import axios from 'axios';
import {Row,Form ,Button} from 'react-bootstrap'


export default class Update extends Component {
    constructor(props){
        super(props);
        this.state={
            data:{
                DATA:[]
            },
            TITLE:'',
            DESCRIPTION:'',
            IMAGE:'',

        }
    }
    componentWillMount(){
        const ID = this.props.match.params.id;
        console.log(this.props.match.params.id);
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${ID}`).then(res=>{  
            this.setState({
                data: res.data.DATA,
                // TITLE: res.data.DATA.TITLE,
                // DESCRIPTION: res.data.DATA.DESCRIPTION,
                // IMAGE: res.data.DATA.IMAGE

            }) ;
            console.log(this.state.data)
            // console.log(this.state.TITLE)
            // console.log(this.state.DESCRIPTION)
                    
            
        })
    }
    // handleUpdate=(e)=>{
    //     const update ={
    //         TITLE:this.state.TITLE,
    //         DESCRIPTION:this.state.DESCRIPTION,
    //         IMAGE:this.state.IMAGE,        
    //     }
    //     axios.put('http://110.74.194.124:15011/v1/api/articles/ ',update).then((res)=>{
    //         this.setState({
                
    //         })
    //     })
    // }
    changeHandler= (e) => {
        this.setState({ [e.target.name]: e.target.value}
            )
    }       
    render() {
        return (
            <div className ="container">
               
                    <Form onSubmit ={(e)=>this.handleUpdate(e)}>
                        <h4>Add New Articles</h4>
                        <Row>
                        <div className="col-lg-8 col-md-8 col-sm-12">
                            <Form.Group controlId="formGroupTitle">
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control type="text" placeholder="Enter title" name="TITLE" value={this.state.TITLE} onChange={(e)=>this.changeHandler(e)}/>
                            </Form.Group>
                            <Form.Group controlId="formGroupDescription">
                                <Form.Label>DESCRIPTION</Form.Label>
                                <Form.Control type="text" placeholder="Enter description" name="DESCRIPTION" value={this.state.DESCRIPTION} onChange={(e)=>this.changeHandler(e)}/>
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Update
                            </Button>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12">
                            <Form.Group>
                                <Form.File id="exampleFormControlFile1" label="File input" />
                            </Form.Group>
                            </div>
                            </Row>
                    </Form>
                
               
            </div>
        )
    }
}
