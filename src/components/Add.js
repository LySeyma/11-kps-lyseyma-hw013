import React, { Component } from 'react';
import axios from 'axios';
import {Row,Form ,Button} from 'react-bootstrap'

export default class Add extends Component {
    constructor() {
        super();
        this.state = {
            data:{
                DATA:[]
            },
            TITLE: '',
            DESCRIPTION: '',
            IMAGE: null,
            errors: {}
        };
        this.changeHandler=this.changeHandler.bind(this);
        this.submitHandler=this.submitHandler.bind(this);
    }
    changeHandler= (e) => {
        this.setState({ [e.target.name]: e.target.value}
            )
    }   
    handleValidation(){
        let data =this.state.data;
        let errors={};
        let formIsValid = true;
        if(!data["TITLE"]){
            formIsValid = false;
           errors["TITLE"] = "Cannot be empty";
        }
        if(typeof data["TITLE"] !== "undefined"){
            if(!data["TITLE"].match(/^[a-zA-Z]+$/)){
               formIsValid = false;
               errors["TITLE"] = "*Please enter your Title.";
            }        
         }
         if(!data["DESCRIPTION"]){
            formIsValid = false;
           errors["DESCRIPTION"] = "Cannot be empty";
        }
        if(typeof data["DESCRIPTION"] !== "undefined"){
            if(!data["DESCRIPTION"].match(/^[a-zA-Z]+$/)){
               formIsValid = false;
               errors["DESCRIPTION"] = "*Please enter your description.";
            }        
         }
         this.setState({errors: errors});
       return formIsValid;
        
    }

    submitHandler = e => {
        e.preventDefault()
        // if(this.handleValidation()){
        //     let data={};
        //     data["TITLE"]="";
        //     data["DESCRIPTION"]="";
        //     this.setState({data:data});
            var addnew ={
                TITLE:this.state.TITLE,
                DESCRIPTION:this.state.DESCRIPTION,
                IMAGE:this.state.IMAGE,        
            // } 
        } 
            axios.post('http://110.74.194.124:15011/v1/api/articles',addnew)
            .then(response => {
                
                console.log(response.data.DATA);
                window.history.back();
 
                alert(response.data.MESSAGE);

            })
            .catch(error => {
                console.log(error)
            })    
    // }
        
        
            
    }  
    onImageChange = (e) => {
        console.log(e);
        if (e.target.files && e.target.files[0]) {
          this.setState({
            IMAGE: URL.createObjectURL(e.target.files[0]),
          });
        }
      };  

    render() {
        return (
            <div className ="container">
               
                    <Form onSubmit ={(e)=>this.submitHandler(e)}>
                        <h4>Add New Articles</h4>
                        <Row>
                        <div className="col-lg-8 col-md-8 col-sm-12">
                            <Form.Group controlId="formGroupTitle">
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control type="text" placeholder="Enter title" name="TITLE" value={this.state.TITLE} onChange={(e)=>this.changeHandler(e)}/>
                            </Form.Group>
                            <Form.Group controlId="formGroupDescription">
                                <Form.Label>DESCRIPTION</Form.Label>
                                <Form.Control type="text" placeholder="Enter description" name="DESCRIPTION" value={this.state.DESCRIPTION} onChange={(e)=>this.changeHandler(e)}/>
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12">
                            <Form.Group>
                                <img src={this.state.IMAGE} style={{width:"200px", height:"200px"}}/>
                                <Form.File id="exampleFormControlFile1" label="File input" name="IMAGE" onChange={this.onImageChange}
                                // onChange={(e)=>this.onImageChange(e)}
                                 />
                            </Form.Group>
                            </div>
                            </Row>
                    </Form>
                
               
            </div>
        )
    }
}
