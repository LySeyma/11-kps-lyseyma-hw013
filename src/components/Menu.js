import React, { Component } from 'react';
import {Nav,Navbar,Form,FormControl,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default class Menu extends Component {
    render() {
        return (
            <div className="App">   
                <Navbar bg="light" variant="light">
                    <div  className="container" >
                    <Navbar.Brand as={Link} to="/">AWS</Navbar.Brand>
                    <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/home">Home</Nav.Link>                  
                    </Nav>
                    <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-primary">Search</Button>
                    </Form>
                    </div>
                </Navbar>
            
            </div>
        )
    }
}
