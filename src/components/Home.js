import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Table, Button} from 'react-bootstrap';
import axios from 'axios';
import Page from './Page';

export default class Home extends Component {
    constructor(){
        super();
        this.state={
            users:[],           
        };        
    }
    componentWillMount(){
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{           
            this.setState({
                users:res.data.DATA,   
            });   
        });       
    }
    componentWillUpdate(){
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{           
            this.setState({
                users:res.data.DATA,               
            });
            // console.log(this.state.CREATED_DATE)            
        });
    }
    handelDelete = (id)=>{
        axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then((res)=>{
            this.componentWillMount();            
            alert(res.data.MESSAGE);
        })
        return axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{           
            
        });
    }
    convertDate = (dateStr) => {
        var dateString = dateStr;
        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);
        var day = dateString.substring(6, 8);
        var date = year + "-" + month + "-" + day;
        return date;
      };
      onChangePage = (pageOfItems) => {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
      }
    render() {
        return (
            <div className ="container">
                <h1>Article Mangement</h1>
                <Link to={`/addNew`} className="btn btn-dark">
                        Add New Article
                </Link><br/><br/>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>CREATE DATE</th>
                        <th>IMAGE</th>
                        <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.users.map((user)=>(
                            <tr key={user.id}>
                                <td >{user.ID}</td>
                                <td style={{width:'20%'}}>{user.TITLE}</td>
                                <td style={{width:'25%'}}>{user.DESCRIPTION}</td>
                                <td style={{width:'15%'}}>{this.convertDate(user.CREATED_DATE)}</td>
                                <td style={{width:'10%'}}><img src={user.IMAGE} alt="Not show" style={{width:"200px", height:"200px"}} /></td>       
                                <td style={{width:'40%'}}>
                                 <Link to={`/view/${user.ID}`} className="btn btn-primary">View</Link>
                                <Link to={`/edit/${user.ID}`} className="btn btn-warning mx-2">Edit</Link> 
                                <Button className="btn btn-danger" onClick={(id)=>this.handelDelete(user.ID)}>Delete</Button>
                                </td>
                            </tr> 
                                                    
                        )  
                        )}    
                    </tbody>
                </Table>
                {/* <Pagination users={this.state.userList} onChangePage={this.onChangePage} pageSize={5} initialPage={1}/> */}
                <Page  users={this.state.DATA} onChangePage={this.onChangePage} pageSize={5} initialPage={1}/>
            </div>
        )
    }
}
