import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Menu from './components/Menu';
import Home from './components/Home'
import Add from './components/Add';
import View from './components/View';
import Update from './components/Update';
import Edit from './components/Edit';

function App() {
  return (
    <div >
          <BrowserRouter>
          <Menu/>
         
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/home" component={Home}/>
          <Route path="/addNew" component={Add}/> 
          <Route path="/view/:id" component={View}/>
          <Route path="/edit/:id" component={Edit}/>
          {/* <Route path="/ii/:id" component={Edit}/> */}
          {/* <Route path="*" component={Error}/> */}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
